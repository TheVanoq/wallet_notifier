from datetime import datetime
import random
import requests
import time
import os

from slack import WebClient
from slack.errors import SlackApiError

SLEEP_TIMER = 30

SLACK_OAUTH_TOKEN = "xoxb-1177873620611-1190297717729-F46mUXqZvTD6BV8IAaBcgTEk"
SLACK_CHANNEL_ID  = "G015E1U0ADA"

TIMESTAMP_STRING_FORMAT = "%H:%M:%S on %d-%b-%Y"

HEARTBEAT_INTERVAL_IN_SECONDS = float(60 * 60 * 24 * 7)


ALL_WALLETS = {
        "TEST1": { "type": "TEST1", "network": "BTCTEST", "desired_balance": "0.02308565", "address": "tb1qy4mrmaraz86008tfwe5jegjh64hflwk6m3u5kg"},
        
        
        #"realpublic":  { "type":  "REALPUBLIC", "network": "BTC",     "desired_balance": "0.02494438", "address": "bc1qx8mtp7tlw9j5jtkkej6qxkpmgtrfkwnsezmkte"},
        #"hidden": { "type": "HIDDEN", "network": "BTC",     "desired_balance": "0.0", "address": "hidden-addr"},
    }

def get_current_timestamp():
    return datetime.now()

def get_current_timestamp_formatted():
    return get_current_timestamp().strftime(TIMESTAMP_STRING_FORMAT)


def send_error_message(error_text, resend_on_error = True):
    error_text = str("ERROR: " + error_text)

    print(error_text)
    
    slack_client = WebClient(token=SLACK_OAUTH_TOKEN)

    response = slack_client.chat_postMessage(channel=SLACK_CHANNEL_ID, text=error_text)

    if not response["message"]["text"] == error_text:
        if resend_on_error:
            print("ERROR: Failed to send the error message to Slack ... resending one time")
            send_error_message(error_text, False)
            

def send_slack_message(message_text, ignore_error = False):
    try:
        slack_client = WebClient(token=SLACK_OAUTH_TOKEN)
        
        response = slack_client.chat_postMessage(channel=SLACK_CHANNEL_ID, text=message_text)

        if not response["message"]["text"] == message_text:
            if not ignore_error:
                send_error_message("The message we sent is broken in some way. Response message != message_text")

    except SlackApiError as error:
        error_message = str("We encountered a SlackAPIError. Code: " + error.response["error"])
        print(error_message)

        if not ignore_error:
            print("Not ignoring the error. Re-sending the message to Slack channel")
            send_slack_message(error_message, True)
        else:
            assert error.response["ok"] is False
            assert error.response["error"]
        

def check_wallet(wallet_type, wallet_network, wallet_addr, desired_balance_str):
    print("Now checking wallet \"" + wallet_type + "\"")
    
    desired_balance = float(desired_balance_str)
    
    response = requests.get("https://sochain.com/api/v2/get_address_balance/" + wallet_network + "/" + wallet_addr + "/0")
        
    if response.status_code == 200:
        content = response.json()

        current_confirmed_balance   = float(content["data"]["confirmed_balance"])
        current_unconfirmed_balance = float(content["data"]["unconfirmed_balance"])
        current_balance = float(current_confirmed_balance + current_unconfirmed_balance)

        if current_balance != float(desired_balance):
            # THE BALANCE HAS CHANGED
            # Send multiple notifications
                
            balance_difference = current_balance - desired_balance

            send_slack_message("WARNING in: " + wallet_type)
            time.sleep(1)
                
            send_slack_message("Activity at: " + get_current_timestamp_formatted())
            time.sleep(1)

            message_text = str("Funds have been moved. Desired balance: " + str(desired_balance) + ". Current balance: " + str(current_balance) + ". Difference: " + str(balance_difference))
            send_slack_message(message_text)
            print(message_text)
            
            send_slack_message("--------------------------------")
        else:
            print("No changes in wallet.")
    else:
        if response.status_code == 404:
            print("Error 404: The Wallet was not found in this network: " + wallet_network + " -> " + wallet_addr)
        elif response.status_code == 429:
            print("Error 429: Request to API failed - Too many requests, slow down the interval")
        else:
            print("Error " + str(response.status_code) + ": Unhandled API Status Code returned")
        

def main():
    time_of_last_heartbeat = get_current_timestamp()
    
    while True:
        for wallet, details in ALL_WALLETS.items():
            check_wallet(details["type"], details["network"], details["address"], details["desired_balance"])
            print()
            
            time.sleep(5)

        if (get_current_timestamp() - time_of_last_heartbeat).total_seconds() > HEARTBEAT_INTERVAL_IN_SECONDS:            
            print("We are over the Heartbeat interval. Send a heartbeat to the Slack channel to let the user know that we are still running")

            text_message = str("The bot is still running but has not detected any activity recently. You should check the wallet manually.")
            send_slack_message(text_message)
            
            time_of_last_heartbeat = get_current_timestamp()
        
        time.sleep(SLEEP_TIMER)

if __name__ == "__main__":
    main()
